# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
from datetime import datetime

class DateTimeEncoder(json.JSONEncoder):
    """Convert datetime objects in AWS response to text."""
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)

def Jsonify(data,indent=2):
    return json.dumps(data,cls=DateTimeEncoder,indent=indent)

# then just use Jsonify(aws_output)